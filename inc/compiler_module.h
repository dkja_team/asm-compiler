#ifndef _COMPILER_MODULE__
#define _COMPILER_MODULE__

class op_name_pairs
{
    std::map<std::string, int> pairs;
public:
    op_name_pairs()
    :pairs{
    {"nop", 0x00}, {"NOP", 0x00},
    {"hold", 0x01}, {"HOLD", 0x01},
    {"call", 0x02}, {"CALL", 0x02},
    {"ret", 0x03}, { "RET", 0x03},
    {"jmp",0x04},{"JMP",0x04},
    {"syscall",0x05},{"SYSCALL",0x05},
    {"loadd",0x0a},{"LOADD",0x0a},
    {"loadw",0x0b},{"LOADW",0x0b},
    {"loadb",0x0c},{"LOADB",0x0c},
    {"stored",0x0d},{"STORED",0x0d},
    {"storew",0x0e},{"STOREW",0x0e},
    {"storeb",0x0f},{"STOREB",0x0f},
    {"push",0x10},{"PUSH",0x10},
    {"pop",0x11},{"POP",0x11},
    {"ifb",0x12},{"IFB",0x12},
    {"ife",0x13},{"IFE",0x13},
    {"ifg",0x14},{"IFG",0x14},
    {"ifne",0x15},{"IFNE",0x15},
    {"ifnb",0x16},{"IFNB",0x16},
    {"ifng",0x17},{"IFNG",0x17},
    {"jmt",0x18},{"JMT",0x18},
    {"jmf",0x19},{"JMF",0x19},
    {"loadds",0x1a},{"LOADDS",0x1a},
    {"loadws",0x1b},{"LOADWS",0x1b},
    {"loadbs",0x1c},{"LOADBS",0x1c},
    {"storeds",0x1d},{"STOREDS",0x1d},
    {"storews",0x1e},{"STOREWS",0x1e},
    {"storebs",0x1f},{"STOREBS",0x1f},
    {"add",0x20},{"ADD",0x20},
    {"sub",0x21},{"SUB",0x22},
    {"mul",0x22},{"MUL",0x22},
    {"div",0x23},{"DIV",0x23},
    {"mod",0x24},{"MOD",0x24},
    {"inc",0x25},{"INC",0x25},
    {"dec",0x26},{"DEC",0x26},
    {"shl",0x27},{"SHL",0x27},
    {"shr",0x28},{"SHR",0x28},
    {"and",0x29},{"AND",0x29},
    {"nand",0x2a},{"NAND",0x2a},
    {"or",0x2b},{"OR",0x2b},
    {"nor",0x2c},{"NOR",0x2c},
    {"xor",0x2d},{"XOR",0x2d},
    {"not",0x2e},{"NOT",0x2e},
    }
    {}
    std::map<std::string, int>& get(){
        return pairs;
    }
};

using Byte = unsigned char;

unsigned int read_hex(std::string s);
unsigned int read_dec(std::string s);
unsigned int read_bin(std::string s);
unsigned int read_digit(std::istream& i);
void read_string_db(std::istream& i,std::vector<Byte>& data);
void read_string_dw(std::istream& i,std::vector<Byte>& data);
void read_string_dd(std::istream& i,std::vector<Byte>& data);
void _db(std::istream& i,std::vector<Byte>& data);
void _dw(std::istream& i,std::vector<Byte>& data);
void _dd(std::istream& i,std::vector<Byte>& data);
std::vector<Byte> compile(std::istream& i,std::map<std::string,int> var);

#endif // _COMPILER_MODULE__
