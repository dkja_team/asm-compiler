#ifndef _ADDRESSING__
#define _ADDRESSING__

#include <iostream>
#include <vector>
#include <map>
#include <stdexcept>


class op_size_pairs
{

    std::map<std::string, int> pairs;

public:
    op_size_pairs()
    :pairs{
    {"nop", 1}, {"NOP", 1},
    {"hold", 1}, {"HOLD", 1},
    {"call", 1}, {"CALL", 1},
    {"ret", 1}, { "RET", 1},
    {"jmp",1},{"JMP",1},
    {"syscall",1},{"SYSCALL",1},
    {"loadd",5},{"LOADD",5},
    {"loadw",5},{"LOADW",5},
    {"loadb",5},{"LOADB",5},
    {"stored",5},{"STORED",5},
    {"storew",5},{"STOREW",5},
    {"storeb",5},{"STOREB",5},
    {"push",5},{"PUSH",5},
    {"pop",1},{"POP",1},
    {"ifb",1},{"IFB",1},
    {"ife",1},{"IFE",1},
    {"ifg",1},{"IFG",1},
    {"ifne",1},{"IFNE",1},
    {"ifnb",1},{"IFNB",1},
    {"ifng",1},{"IFNG",1},
    {"jmt",5},{"JMT",5},
    {"jmf",5},{"JMF",5},
    {"add",1},{"ADD",1},
    {"sub",1},{"SUB",1},
    {"mul",1},{"MUL",1},
    {"div",1},{"DIV",1},
    {"mod",1},{"MOD",1},
    {"inc",1},{"INC",1},
    {"dec",1},{"DEC",1},
    {"shl",1},{"SHL",1},
    {"shr",1},{"SHR",1},
    {"and",1},{"AND",1},
    {"nand",1},{"NAND",1},
    {"or",1},{"OR",1},
    {"nor",1},{"NOR",1},
    {"xor",1},{"XOR",1},
    {"not",1},{"NOT",1},
    }
    {}

    std::map<std::string, int>& get(){
        return pairs;
    }
};

int db(std::istream& s);
int dw(std::istream& s);
int dd(std::istream& s);
void digit(std::istream& s);
int calculate_opcodes_size(std::istream& in, std::vector<char> delims);
std::map<std::string, int> calculate_fun_addr(std::stringstream& ss);

#endif  //_ADDRESSING__
