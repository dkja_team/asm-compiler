#ifndef _REMOVE_COMMENTS__
#define _REMOVE_COMMENTS__

#include <string>
#include <istream>
#include <limits>

class remove_comments
{
    std::string data;
public:
    remove_comments(std::istream& in);
    std::string get_stream();

};
#endif // _REMOVE_COMMENTS__
