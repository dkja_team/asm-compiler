;
; this is comment
;
	nop

@section code

$main
	nop		 	; nic nie robi
	push 	5	 	; wrzuć na stos 5
	push 	4	 	; wrzuć na stos 4
	add		 	; dodaj 5 i 4
	push	fun
	call			; dodaje: 9+1
	push	stop_exec
	jmp  			; wywołaj funkcje zwieszającą program
	

$fun			; ta funkcja dodaje do zmiennej 1
	inc		
	ret		

$stop_exec		; tu jest zakończenie programu
	hold

$infinity_loop	; tu jest petla nieskończona która nic nie robi
	push infinity_loop
	jmp



@section data

$var1	db "Hello World";11
;	db 0		;1	
;$var_h	dw 000DE12h	;2
;$vaar 	dd 00001010B	;4
;$var4 	db 1010b	;1


