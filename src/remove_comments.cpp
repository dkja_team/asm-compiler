#include "remove_comments.h"
using namespace std;

remove_comments::remove_comments(istream& in)
{
        int c{};

    while(in)
    {
        c=in.get();
        if(c == EOF)
            break;
        if(c==';')
        {
            in.ignore(numeric_limits<streamsize>::max(),'\n');
            data.push_back('\n');
        }
        else
            data.push_back(static_cast<char>(c));
    }
}
string remove_comments::get_stream()
{
    return data;
}
