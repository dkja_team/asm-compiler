#include <iostream>
#include <vector>
#include <map>
#include <stdexcept>
#include <sstream>
#include "addressing.h"

using namespace std;

int db(istream& s)
{
    s.get();
    int counter{0};
    while(s.get() != '"')
    {
        ++counter;
    }
    return counter;
}

int dw(istream& s)
{
    s.get();
    int counter{0};
    while(s.get() != '"')
    {
        ++counter;
    }
    return counter*2;
}

int dd(istream& s)
{
    s.get();
    int counter{0};
    while(s.get() != '"')
    {
        ++counter;
    }
    return counter*4;
}

void digit(istream& s)
{
    string tmp;
    s >> tmp;
}

int calculate_opcodes_size(istream& in, vector<char> delims)
{
    op_size_pairs op_size;

    int     counter{0};
    char    tmpc;
    string  tmps;

    while(in >> tmpc)
    {
        in.putback(tmpc);
        for(auto a : delims)
            if(a == tmpc)
                return counter;
        in >> tmps;
        if(tmps == "db" || tmps == "DB")
        {
            in >> tmpc;
            in.putback(tmpc);
            if(tmpc == '"')
                counter+=db(in);
            else
            {
                digit(in);
                counter+=1;
            }
            continue;
        }
        else
            if(tmps == "dw" || tmps == "DW")
            {
                in >> tmpc;
                in.putback(tmpc);
                if(tmpc == '"')
                    counter+=dw(in);
                else
                {
                    digit(in);
                    counter+=2;
                }
                continue;
            }
            else
                if(tmps == "dd" || tmps == "DD")
                {
                    in >> tmpc;
                    in.putback(tmpc);
                    if(tmpc == '"')
                        counter+=dd(in);
                    else
                    {
                        digit(in);
                        counter+=4;
                    }
                    continue;
                }
        try{
            int x = op_size.get().at(tmps);
            counter+=x;
            if(x == 5)
                in >> tmps;
        }catch(out_of_range)
        {
            cerr << "Invalid opcode: " << tmps;
            exit(-1);
        }
    }

    return counter;
}

map<string, int> calculate_fun_addr(stringstream& ss)
{
    map<string, int>address{};
    int counter{0};

     while(ss)
    {
        string tmps;
        char tmp;

        ss >> tmp;
        ss.putback(tmp);
        if(tmp == '@')
        {
            ss >> tmps >> tmps;
            continue;
        }else
            if(tmp == '$')
            {
                ss >> tmps;
                tmps.erase(tmps.begin());
                address.insert(pair<string,int>(tmps,counter));

                continue;
            }
        else{
            counter += calculate_opcodes_size(ss, {'@', '$'});
        }
    }
    return address;
}
