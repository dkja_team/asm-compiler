#include <vector>
#include <iostream>
#include <map>
#include <string>
#include "compiler_module.h"

using Byte = unsigned char;
using namespace std;


unsigned int read_hex(string s)
{
    unsigned int res = 0;
    if(s.back() == 'h' || s.back() == 'H')
    {
        for(auto x: s)
            {
                if(x>='0' && x<='9')
                    {
                        res*=16;
                        res+=x - '0';
                    }

                else if(x>='a' && x<='f')
                    {
                        res*=16;
                        res+=x - 'a' + 10;
                    }
                 else if(x>='A' && x<='F')
                    {
                        res*=16;
                        res+=x - 'A' + 10;
                    }
            }
        return res;
    }
    return res;
}

unsigned int read_dec(string s)
{
    unsigned int res = 0;
    for(auto x: s)
            {
                if(x>='0' && x<='9')
                    {
                        res*=10;
                        res+=x - '0';
                    }
            }
    return res;
}

unsigned int read_bin(string s)
{
    unsigned int res = 0;
    for(auto x: s)
            {
                if(x>='0' && x<='1')
                    {
                        res*=2;
                        res+=x - '0';
                    }
            }
    return res;
}

unsigned int read_digit(istream& i)
{
    string tmps;

    i >> tmps;
    if(tmps.size() == 1)
        return read_dec(tmps);

        if(tmps.back() == 'h' || tmps.back() == 'H')
            return read_hex(tmps);
        else if(tmps.back() == 'b' || tmps.back() == 'B')
            return read_bin(tmps);


    return read_dec(tmps);
}

void read_string_db(istream& i,vector<Byte>& data)
{
    unsigned char tmpc;

    i >> tmpc;  // read first "

    while((tmpc = i.get()) != EOF)
    {
        if(tmpc != '"')
        {
            data.push_back(tmpc);
        } else
        {
            return;
        }
    }

        cerr << "Expect \" on end string(that's end of file)";
        throw std::exception{};
}

void read_string_dw(istream& i,vector<Byte>& data)
{
    unsigned char tmpc;

    i >> tmpc;  // read first "

    while((tmpc = i.get()) != EOF)
    {
        if(tmpc != '"')
        {
            data.push_back(tmpc);
            data.push_back(0x00);
        } else
        {
            return;
        }
    }

        cerr << "Expect \" on end string(that's end of file)";
        throw std::exception{};
}

void read_string_dd(istream& i,vector<Byte>& data)
{
    unsigned char tmpc;

    i >> tmpc;  // read first "

    while((tmpc = i.get()) != EOF)
    {
        if(tmpc != '"')
        {
            data.push_back(tmpc);
            data.push_back(0x00);
            data.push_back(0x00);
            data.push_back(0x00);
        } else
        {
            return;
        }
    }

        cerr << "Expect \" on end string(that's end of file)";
        throw std::exception{};
}

void _db(istream& i,vector<Byte>& data)
{
    char   tmpc;
    string tmps;

    i >> tmpc;
    i.putback(tmpc);
    if(tmpc >= '0'&& tmpc <= '9')
    {
        unsigned int tmpi = read_digit(i);
        if(tmpi > 255)
        {
            cerr << "Warning: Value" << tmpi << "is big as byte";
        }
        tmpc = ((tmpi & 0x000000FF));
        data.push_back(tmpc);

    }
    else if(tmpc == '"')
    {
        read_string_db(i, data);
    }
    else
    {
        getline(i, tmps);
        cerr << ": Expect digit or string after db (that's: "<< tmps << ')';
        throw std::exception{};
    }
}

void _dw(istream& i,vector<Byte>& data)
{
    char   tmpc;
    string tmps;

    i >> tmpc;
    i.putback(tmpc);
    if(tmpc >= '0'&& tmpc <= '9')
    {
        unsigned int tmpi = read_digit(i);
        if(tmpi > 65535)
        {
            cerr << "Warning: Value" << tmpi << "is big as word";
        }
        tmpc = ((tmpi & 0x000000FF));
        data.push_back(tmpc);
        tmpc = ((tmpi & 0x0000FF00) >> 8);
        data.push_back(tmpc);

    }
    else if(tmpc == '"')
    {
        read_string_dw(i, data);
    }
    else
    {
        getline(i, tmps);
        cerr << ": Expect digit or string after dw (that's: "<< tmps << ')';
        throw std::exception{};

    }
}

void _dd(istream& i,vector<Byte>& data)
{
    char   tmpc;
    string tmps;

    i >> tmpc;
    i.putback(tmpc);
    if(tmpc >= '0'&& tmpc <= '9')
    {
        unsigned int tmpi = read_digit(i);
        if(tmpi > 4293918719)
        {
            cerr << "Warning: Value" << tmpi << "is big as double word";
        }
                    tmpi = ((tmpi & 0x000000FF));
                    data.push_back(tmpi);
                    tmpi = ((tmpi & 0x0000FF00)>>8);
                    data.push_back(tmpi);
                    tmpi = ((tmpi & 0x00FF0000)>>16);
                    data.push_back(tmpi);
                    tmpi = ((tmpi & 0xFF000000)>>24);
                    data.push_back(tmpi);
    }
    else if(tmpc == '"')
    {
        read_string_dd(i, data);
    }
    else
    {
        getline(i, tmps);
        cerr << ": Expect digit or string after dd (that's: "<< tmps << ')';
        throw std::exception{};

    }
}

vector<Byte> compile(istream& i,map<string,int> var)
{
    vector<Byte> data;
    char    tmpc;
    string  tmps;
    op_name_pairs opc;


    while(i)
    {
        i >> tmpc;
        i.putback(tmpc);
        if(tmpc == '@')
            i >> tmps >> tmps;
        else
            if(tmpc == '$')
            {
                i >> tmps;
            }
        else
        {
            int tmpi;
            Byte x;
            i>>tmps;
            if(tmps == "db" || tmps == "DB")
                {
                    _db(i, data);
                    if(i >> tmpc)
                        i.putback(tmpc);
                    continue;
                }
            else if(tmps == "dw" || tmps == "DW")
                {
                    _dw(i, data);
                    if(i >> tmpc)
                        i.putback(tmpc);
                    continue;
                }
            else if(tmps == "dd" || tmps == "DD")
                {
                    _dd(i, data);
                    if(i >> tmpc)
                        i.putback(tmpc);
                    continue;
                }

            unsigned char opcode = opc.get().at(tmps);
            switch(opcode)
            {
                case 0x0a:
                case 0x0b:
                case 0x0c:
                case 0x0d:
                case 0x0e:
                case 0x0f:
                case 0x10:
                case 0x18:
                case 0x19:
                    data.push_back(opcode);
                    i >> tmpc;
                    i.putback(tmpc);
                    if(tmpc >= '0' && tmpc <='9')
                       tmpi = read_digit(i);
                    else
                    {
                        i >> tmps;
                        tmpi = var.at(tmps);
                    }
                    x = ((tmpi & 0x000000FF));
                    data.push_back(x);
                    x = ((tmpi & 0x0000FF00)>>8);
                    data.push_back(x);
                    x = ((tmpi & 0x00FF0000)>>16);
                    data.push_back(x);
                    x = ((tmpi & 0xFF000000)>>24);
                    data.push_back(x);

                    break;
                default:
                    data.push_back(opcode);
                    break;
            }
            if(i>>tmpc)
                i.putback(tmpc);
        }
    }
    return data;
}
