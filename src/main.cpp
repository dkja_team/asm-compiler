#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <map>
#include <utility>
#include "remove_comments.h"
#include "addressing.h"
#include "compiler_module.h"


using namespace std;
using Byte = unsigned char;


int main(int argc, char** argv)
{
    vector<string> args{};
    {
        for(int x = 0; x < argc ; ++x)
            args.push_back(argv[x]);
    }

    if( args.size() < 3 )
    {
        cerr << "USAGE: \n\t"<<args.at(0) << "source_file out_file";
    }

    ifstream i(args.at(1));
    if(!i.good())
        {
            cerr << "Cannot open: " << args.at(1);
            exit(-1);
        }
    ofstream ofile(args.at(2));
    if(!ofile.good())
        {
            cerr << "Cannot open: " << args.at(2);
            exit(-1);
        }
    remove_comments without_comm{i};
    string source_code = without_comm.get_stream();
    stringstream ss {source_code};
    stringstream ss2 {source_code};
    map<string, int> address{calculate_fun_addr(ss)};
    vector<Byte> compiled=compile(ss2,address);


    for(auto x: address)
        cout << x.first << ": " << x.second << '\n';
    cerr << "Compiled: "<< compiled.size() << "bytes\n";

    ofile.write(reinterpret_cast<const char*>(compiled.data()), compiled.size());

    ofile.close();
    i.close();

    return 0;
}
